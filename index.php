<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <!-- The above 3 meta tags *must* come first in the head; any other head content must come *after* these tags -->
    <title>MWDS</title>

    <!-- Bootstrap -->
    <link href="bs/css/bootstrap.min.css" rel="stylesheet">
    <link href="styles.css" rel="stylesheet">

    <!-- HTML5 shim and Respond.js for IE8 support of HTML5 elements and media queries -->
    <!-- WARNING: Respond.js doesn't work if you view the page via file:// -->
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/html5shiv/3.7.3/html5shiv.min.js"></script>
    <script src="https://oss.maxcdn.com/respond/1.4.2/respond.min.js"></script>
    <![endif]-->
</head>
<body>
<div class="page">
    <div class="col-md-3 side">
        <img src="logo2.png" class="img-responsive">
        <br><BR>
        <iframe id="forecast_embed" type="text/html" frameborder="0" height="125" width="75%"
                src="http://forecast.io/embed/#lat=42.2923&lon=-83.7145&name=Ann Arbor, MI&color=#00aaff&font=Helvetica&units=us">
        </iframe>
        <a class="twitter-timeline" data-dnt="true" href="https://twitter.com/search?q=%23drupal%20OR%20%20%23mwds"
           data-widget-id="765934147772612608">Tweets about #drupal OR #mwds</a>
        <script>!function (d, s, id) {
            var js, fjs = d.getElementsByTagName(s)[0], p = /^http:/.test(d.location) ? 'http' : 'https';
            if (!d.getElementById(id)) {
                js = d.createElement(s);
                js.id = id;
                js.src = p + "://platform.twitter.com/widgets.js";
                fjs.parentNode.insertBefore(js, fjs);
            }
        }(document, "script", "twitter-wjs");</script>


    </div>

    <div class="col-md-8	main">
        <div class="intro">
              <h2 > Welcome</h2>
        </div>
        <div id="myCarousel" class="carousel slide" data-ride="carousel">
            <!-- Indicators -->
            <ol class="carousel-indicators">
                <li data-target="#myCarousel" data-slide-to="0" class="active"></li>
                <li data-target="#myCarousel" data-slide-to="1"></li>
                <li data-target="#myCarousel" data-slide-to="2"></li>
                <li data-target="#myCarousel" data-slide-to="3"></li>
                <li data-target="#myCarousel" data-slide-to="4"></li>

            </ol>

            <!-- Wrapper for slides -->
            <div class="carousel-inner" role="listbox">
                <div class="item active">

                    <h1>Wireless</h1>
                    <h3>SSID: MWireless</h3>
                    <h3>Username: um171186</h3>
                    <h3>Password : Viewsis38878!</h3>

                </div>

                <div class="item">
                    <h1>Please get your lunch order form to Michael by 10:45 AM</h1>
                </div>

                <div class="item">
                    <h1>Sponsors</h1>
                    <p>Anna Kalata</p>
                    <p> Frank Albanese</p>
                    <p> Sam Keith</p>
                    <p>Michael Hess</p>
                </div>

                <div class="item">
                   <img src="logo.png">
                </div>

            </div>

            <!-- Left and right controls -->

        </div>
    </div>
</div>
<!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.12.4/jquery.min.js"></script>
<!-- Include all compiled plugins (below), or include individual files as needed -->
<script src="bs/js/bootstrap.min.js"></script>
<script>
    $('.carousel').carousel({
        interval: 10000
    });
    setTimeout(function () {
        window.location.reload(1);
    }, 500000);

</script>
</body>
</html>